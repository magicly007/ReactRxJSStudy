/*
 @jsx
 */
var data = [
    {author: "Pete Hunt", text: "This is one comment"},
    {author: "Jordan Walke", text: "This is *another* comment"}
];

var CommentBox = React.createClass({
    getInitialState: function () {
        return {data: []};
    },
    loadCommentsFromServer: function () {
        jQuery.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function (data) {
                this.setState({data: data});
            }.bind(this),
            error: function (xhr, status, err) {
                console.log(this.props.url, status, error.toString())
            }.bind(this)
        });
    },
    componentDidMount: function () {
        this.loadCommentsFromServer();
        setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },
    handleCommentSubmit: function (comment) {
        console.log(comment)
        console.log(this.state.data)
        this.setState({data: this.state.data.concat(comment)});
    },
    render: function () {
        return (
            <div className="commentBox">
                <h1>Comments</h1>
                <CommentList data={this.state.data}/>
                <CommentForm onCommentSubmit={this.handleCommentSubmit}/>
            </div>
        );
    }
});

var CommentList = React.createClass({
    render: function () {
        var v = this.props.data.map(function (comment) {
            return (
                <Comment author={comment.author}> {comment.text } </Comment >
            );
        });
        return (
            <div className="commentList">
                {v}
            </div>
        );
    }
});
var Comment = React.createClass({
    render: function () {
        var rawHtml = marked(this.props.children.toString(), {sanitize: true});
        return (
            <div
                className="comment">
                <h2 className="commentAuthor"> {this.props.author} </h2 >
                <span dangerouslySetInnerHTML={{ __html: rawHtml }}/>
            </div>
        );
    }
});
var CommentForm = React.createClass({
    handleSubmit: function (e) {
        e.preventDefault();
        var author = React.findDOMNode(this.refs.author).value.trim();
        var text = React.findDOMNode(this.refs.text).value.trim();
        if (!text || !author) {
            return;
        }
        this.props.onCommentSubmit({author: author, text: text});
        React.findDOMNode(this.refs.author).value = '';
        React.findDOMNode(this.refs.text).value = '';
    },
    render: function () {
        return (
            <form className="commentForm" onSubmit={this.handleSubmit }>
                < input type="text" placeholder="Your Name" ref="author"/>
                < input type="text" placeholder="Say sth..." ref="text"/>
                < input type="submit" value="Post"/>
            </form>
        );
    }
});

React.render(< CommentBox url="js/comments.json" pollInterval={2000}/>, document.getElementById("content"));
