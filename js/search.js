var ProductCategoryRow = React.createClass({
    render: function () {
        return (<tr>
            <th colSpan="2">{this.props.category}</th>
        </tr>);
    }
});

var ProductRow = React.createClass({
    render: function () {
        var name = this.props.product.stocked ?
            this.props.product.name :
            <span style={{color: 'red'}}>
        {this.props.product.name}
    </span>;
        return (
            <tr>
                <td>{name}</td>
                <td>{this.props.product.price}</td>
            </tr>
        );
    }
});

var ProductTable = React.createClass({
    render: function () {
        var rows = [];
        var lastCategory = null;
        this.props.products.forEach(function (product) {
            if (product.category !== lastCategory) {
                rows.push(<ProductCategoryRow category={product.category} key={product.category}/>);
            }
            rows.push(<ProductRow product={product} key={product.name}/>);
            lastCategory = product.category;
        });
        return (
            <table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
});

var SearchBar = React.createClass({
    onChange: function () {
        var text = React.findDOMNode(this.refs.filterText).value.trim()
        var inStock = React.findDOMNode(this.refs.inStock).checked
        console.log(text + " " + inStock)
        this.props.onChange({filterText: text, inStockOnly: inStock})
    },
    render: function () {
        return (
            <form>
                <input type="text" placeholder="Search..." value={this.props.filterText} onChange={this.onChange}
                       ref="filterText"/>

                <p>
                    <input type="checkbox" checked={this.props.inStockOnly} onChange={this.onChange} ref="inStock"/>
                    {' '}
                    Only show products in stock
                </p>
            </form>
        );
    }
});

var FilterableProductTable = React.createClass({
    getInitialState: function () {
        return {
            filterText: '',
            inStockOnly: false
        };

    },
    onChange: function (data) {
        console.log(data)
        console.log({filterText: data.filterText, inStockOnly: data.inStockOnly});
        this.setState({filterText: data.filterText, inStockOnly: data.inStockOnly});
        console.log("onChange:" + this.state.filterText + " " + this.state.inStockOnly);
    },
    render: function () {
        var filterProducts = this.props.products.filter(function (p) {
            console.log("render: " + this.state.filterText)
            return p.name.indexOf(this.state.filterText) != -1 &&
                (!this.state.inStockOnly || p.stocked)
            //return true;
        }.bind(this));
        return (
            <div>
                <SearchBar filterText={this.state.filterText} inStockOnly={this.state.inStockOnly}
                           onChange={this.onChange}/>
                <ProductTable products={filterProducts}/>
            </div>
        );
    }
});


var PRODUCTS = [
    {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
    {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
    {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
    {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
    {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
    {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];

React.render(<FilterableProductTable products={PRODUCTS}/>, document.body);
